var saveUpdateIndex = 0;
var staffList = [];
const STAFFLIST = "STAFFLIST";
var localstorageStafflist = localStorage.getItem("STAFFLIST");
if (JSON.parse(localstorageStafflist)) {
  // convert json sang array
  var data = JSON.parse(localstorageStafflist);
  for (var index = 0; index < data.length; index++) {
    var curent = data[index];
    var staff = new Staff(
      curent.taiKhoan,
      curent.hoTen,
      curent.email,
      curent.matKhau,
      curent.ngayLam,
      curent.luongCoBan,
      curent.chucVu,
      curent.gioLam
    );
    staffList.push(staff);
  }
  renderStaff(staffList);
}
function saveLocalStorage() {
  // chuyển array thành json
  var staffListJson = JSON.stringify(staffList);
  // lưu xuống localstorage
  localStorage.setItem(STAFFLIST, staffListJson);
}
function addStaff() {
  var newStaff = getInfor();
  //   kiểm tra tài khoản nhân viên
  var isValid =
    validator.kiemTraRong(
      newStaff.taiKhoan,
      "tbTKNV",
      "Tài khoản không được để trống"
    ) &&
    validator.kiemTraDoDai(newStaff.taiKhoan, "tbTKNV", 4, 6) &&
    validator.kiemTraTaiKhoan(newStaff.taiKhoan, "tbTKNV", staffList);

  //   kiểm tra họ và tên nhân viên
  isValid =
    isValid &
    (validator.kiemTraRong(
      newStaff.hoTen,
      "tbTen",
      "Họ và tên không được để rỗng"
    ) && validator.kiemTraChuoiTen(newStaff.hoTen, "tbTen"));
  // kiểm tra emai
  isValid =
    isValid &
    (validator.kiemTraRong(
      newStaff.email,
      "tbEmail",
      "Email không được để rỗng"
    ) && validator.kiemTraChuoiEmail(newStaff.email, "tbEmail"));
  // Kiểm tra mật khẩu
  isValid =
    isValid &
    (validator.kiemTraRong(
      newStaff.matKhau,
      "tbMatKhau",
      "Mật khẩu không được để rỗng"
    ) && validator.kiemTraChuoiMatKhau(newStaff.matKhau, "tbMatKhau"));
  // kiểm tra ngày tháng năm
  isValid =
    isValid &
    (validator.kiemTraRong(
      newStaff.ngayLam,
      "tbNgay",
      "Ngày làm không được để trống"
    ) && validator.kiemTraChuoiNgayThangNam(newStaff.ngayLam, "tbNgay"));
  // Kiểm tra lương cơ bản
  isValid =
    isValid &
    (validator.kiemTraRong(
      newStaff.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản không được để trống"
    ) && validator.kiemTraLuongCoBan(newStaff.luongCoBan, "tbLuongCB"));
  // Kiểm tra chức vụ
  isValid = isValid & validator.kiemTraChucVu(newStaff.chucVu, "tbChucVu");
  //   Kiểm tra giờ làm
  isValid =
    isValid &
    (validator.kiemTraRong(
      newStaff.gioLam,
      "tbGiolam",
      "Giờ làm không được để rỗng"
    ) && validator.kiemTraGioLam(newStaff.gioLam, "tbGiolam"));
  //   kiểm tra điều kiện
  if (isValid == false) {
    return;
  }
  staffList.push(newStaff);
  saveLocalStorage();
  renderStaff(staffList);
}

function deleteStaff(taiKhoan) {
  index = findIndexStaff(taiKhoan, staffList);
  if (index != -1) {
    staffList.splice(index, 1);
    saveLocalStorage();
    renderStaff(staffList);
  }
}
function fixStaffInfor(taiKhoan) {
  index = findIndexStaff(taiKhoan, staffList);
  showInfor(staffList[index]);
  saveUpdateIndex = index;
}
function updateStaff() {
  var staffEdited = getInfor();
  //   kiểm tra tài khoản nhân viên
  var isValid =
    validator.kiemTraRong(
      staffEdited.taiKhoan,
      "tbTKNV",
      "Tài khoản không được để trống"
    ) &&
    validator.kiemTraDoDai(staffEdited.taiKhoan, "tbTKNV", 4, 6) &&
    validator.kiemTraCapNhatTaiKhoan(
      staffEdited.taiKhoan,
      "tbTKNV",
      staffList[saveUpdateIndex].taiKhoan
    );

  //   kiểm tra họ và tên nhân viên
  isValid =
    isValid &
    (validator.kiemTraRong(
      staffEdited.hoTen,
      "tbTen",
      "Họ và tên không được để rỗng"
    ) && validator.kiemTraChuoiTen(staffEdited.hoTen, "tbTen"));
  // kiểm tra emai
  isValid =
    isValid &
    (validator.kiemTraRong(
      staffEdited.email,
      "tbEmail",
      "Email không được để rỗng"
    ) && validator.kiemTraChuoiEmail(staffEdited.email, "tbEmail"));
  // Kiểm tra mật khẩu
  isValid =
    isValid &
    (validator.kiemTraRong(
      staffEdited.matKhau,
      "tbMatKhau",
      "Mật khẩu không được để rỗng"
    ) && validator.kiemTraChuoiMatKhau(staffEdited.matKhau, "tbMatKhau"));
  // kiểm tra ngày tháng năm
  isValid =
    isValid &
    (validator.kiemTraRong(
      staffEdited.ngayLam,
      "tbNgay",
      "Ngày làm không được để trống"
    ) && validator.kiemTraChuoiNgayThangNam(staffEdited.ngayLam, "tbNgay"));
  // Kiểm tra lương cơ bản
  isValid =
    isValid &
    (validator.kiemTraRong(
      staffEdited.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản không được để trống"
    ) && validator.kiemTraLuongCoBan(staffEdited.luongCoBan, "tbLuongCB"));
  // Kiểm tra chức vụ
  isValid = isValid & validator.kiemTraChucVu(staffEdited.chucVu, "tbChucVu");
  //   Kiểm tra giờ làm
  isValid =
    isValid &
    (validator.kiemTraRong(
      staffEdited.gioLam,
      "tbGiolam",
      "Giờ làm không được để rỗng"
    ) && validator.kiemTraGioLam(staffEdited.gioLam, "tbGiolam"));
  //   kiểm tra điều kiện
  if (isValid == false) {
    return;
  }
  var index = findIndexStaff(staffEdited.taiKhoan, staffList);
  if (index != -1) {
    staffList[index] = staffEdited;
    saveLocalStorage();
    renderStaff(staffList);
  }
}

function searchStaff() {
  var curentList = [];
  var char = document.getElementById("searchName").value;
  console.log("char: ", char);
  for (var index = 0; index < staffList.length; index++) {
    var curentStaff = staffList[index];
    console.log("curentStaff: ", curentStaff.xepLoai());
    var fullChar = getChar(char, curentStaff.xepLoai());
    if (char == fullChar) {
      curentList.push(curentStaff);
    }
  }
  renderStaff(curentList);
  console.log("curentList: ", curentList);
}
