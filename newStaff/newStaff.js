function Staff(
  taiKhoan,
  hoTen,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.hoTen = hoTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      var tongLuong = this.luongCoBan * 3;
    }
    if (this.chucVu == "Trưởng phòng") {
      var tongLuong = this.luongCoBan * 2;
    }
    if (this.chucVu == "Nhân viên") {
      var tongLuong = this.luongCoBan * 1;
    }
    return tongLuong.toLocaleString();
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      var xepLoai = "Xuất sắc";
    } else if (this.gioLam >= 176) {
      var xepLoai = "Giỏi";
    } else if (this.gioLam >= 160) {
      var xepLoai = "Khá";
    } else {
      var xepLoai = "Trung bình";
    }
    return xepLoai;
  };
}
