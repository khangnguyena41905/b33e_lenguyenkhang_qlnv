function getInfor() {
  var taiKhoan = document.getElementById("tknv").value.trim();
  var hoTen = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value.trim();
  var ngayLam = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value.trim();
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value.trim();

  var staff = new Staff(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  return staff;
}

function renderStaff(list) {
  var contentHTML = "";
  list.forEach((item) => {
    var content = `<tr>
        <td>${item.taiKhoan}</td>
        <td>${item.hoTen}</td>
        <td>${item.email}</td>
        <td>${item.ngayLam}</td>
        <td>${item.chucVu}</td>
        <td>${item.tongLuong()}</td>
        <td>${item.xepLoai()}</td>
        <td>
        <button class="btn btn-danger" onclick="deleteStaff('${
          item.taiKhoan
        }')">Xoá</button>
        <button class="btn btn-warning" data-toggle="modal"
        data-target="#myModal" onclick="fixStaffInfor('${
          item.taiKhoan
        }')" >Sửa</button>
        </td>
        </tr>`;
    contentHTML += content;
  });
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function findIndexStaff(taiKhoan, arr) {
  return arr.findIndex(function (staff) {
    return staff.taiKhoan == taiKhoan;
  });
}

function showInfor(staff) {
  document.getElementById("tknv").value = staff.taiKhoan;
  document.getElementById("name").value = staff.hoTen;
  document.getElementById("email").value = staff.email;
  document.getElementById("password").value = staff.matKhau;
  document.getElementById("datepicker").value = staff.ngayLam;
  document.getElementById("luongCB").value = staff.luongCoBan;
  document.getElementById("chucvu").value = staff.chucVu;
  document.getElementById("gioLam").value = staff.gioLam;
}
function getChar(char, string) {
  var fullChar = "";
  for (var index = 0; index < char.length; index++) {
    var curentChar = string[index];
    fullChar += curentChar;
  }
  return fullChar;
}
