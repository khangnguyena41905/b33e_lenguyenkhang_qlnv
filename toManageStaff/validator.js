var validator = {
  kiemTraRong: function (valueInput, idError, message) {
    if (valueInput == "") {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraDoDai: function (valueInput, idError, min, max) {
    if (valueInput.length < min || valueInput.length > max) {
      document.getElementById(idError).innerText = `Độ dài từ ${min} - ${max}`;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraTaiKhoan: function (valueInput, idError, staffList) {
    var index = staffList.findIndex(function (staff) {
      return staff.taiKhoan == valueInput;
    });
    if (index != -1) {
      document.getElementById(idError).innerText = "Tài khoản đã tồn tại";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuoiTen: function (valueInput, idError) {
    var regex =
      /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễếệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u;
    if (regex.test(valueInput) == false) {
      document.getElementById(idError).innerText = "Họ tên phải là chữ";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuoiEmail: function (valueInput, idError) {
    var regex = /\S+@\S+\.\S+/;
    if (regex.test(valueInput) == false) {
      document.getElementById(idError).innerText = "Email sai cú pháp";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuoiMatKhau: function (valueInput, idError) {
    var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    if (regex.test(valueInput) == false) {
      document.getElementById(idError).innerText =
        "Mật khẩu phải từ 6-10 ký tự và có ít nhất 1 chữ in hoa, 1 chữ số, 1 ký tự đặt biệt";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuoiNgayThangNam: function (valueInput, idError) {
    var regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/\d{4}$/;
    if (regex.test(valueInput) == false) {
      document.getElementById(idError).innerText =
        "Ngày tháng năm phải theo định dạng mm/dd/yyyy";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraLuongCoBan: function (valueInput, idError) {
    if (valueInput * 1 < 1000000 || valueInput * 1 > 20000000) {
      document.getElementById(idError).innerText =
        "Lương cơ bản từ 1,000,000-20,000,000";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChucVu: function (valueInput, idError) {
    if (valueInput == 0) {
      document.getElementById(idError).innerText = "Hãy chọn chức vụ";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraGioLam: function (valueInput, idError) {
    if (valueInput < 80 || valueInput > 200) {
      document.getElementById(idError).innerText = "Giờ làm phải từ 80-200 giờ";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraCapNhatTaiKhoan: function (valueInput, idError, curentId) {
    if (valueInput != curentId) {
      document.getElementById(idError).innerText =
        "Không được thay đổi tài khoản";
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
};
